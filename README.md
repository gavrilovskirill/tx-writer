## Transactions writer to DB

### Start app:
```
npm install

or

yarn install
```

Copy .env file from .env.example and set your data for database connection.
```
cp .env.example .env
```

Perform migrations
```
npm run db:migrate

or 

yarn run db:migrate
```

and run you application
```
npm run start

or 

yarn run start
```

swagger address:
```
http://localhost:3000/swagger
```
