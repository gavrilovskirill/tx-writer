import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AddressesService } from './addresses.service';
import { TAddress } from './interfaces/IAddresses'

@Controller('addresses')
export class AddressesController {
    constructor(private readonly addressesService: AddressesService) {}

    @Get('/changed-most')
    @ApiTags('Addresses')
    getChangedMost():Promise<TAddress> {
        return this.addressesService.getChangedMost();
    }

}
