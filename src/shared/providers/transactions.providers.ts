import { Transaction } from '../entity/Transaction';

export const TransactionsProviders = [
    { provide: 'TransactionRepository', useValue: Transaction },
];
