import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddBlock {
    @ApiProperty()
    @IsNumber()
    blockNumber: number;

    @ApiProperty()
    @IsString()
    blockHash: string;
}
