import { Module } from '@nestjs/common';
import { SharedModule } from './shared/shared.module';
import { AddressesModule } from './addresses/addresses.module';
import { TxWriterModule } from './services/tx-writer/tx-writer.module';

@Module({
    imports: [
        SharedModule,
        AddressesModule,
        TxWriterModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
