export interface IAddresses {
  getChangedMost(): Promise<TAddress>
}

export type TAddress = {
  address: string,
  value: number,
}

export interface IAddress {
  address: string,
  value: number,
}
