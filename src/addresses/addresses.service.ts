import { Inject, Injectable } from '@nestjs/common';
import { IAddresses, TAddress } from './interfaces/IAddresses';
import { Transaction } from '../shared/entity/Transaction';
import { get } from 'lodash';
import { QueryTypes } from 'sequelize';

const COUNT_LAST_BLOCK = 100;

@Injectable()
export class AddressesService implements IAddresses {
    constructor(
        @Inject('TransactionRepository')
        private readonly repository: typeof Transaction,
    ) {}

    async getChangedMost(): Promise<TAddress> {
        const resultFrom: TAddress[] = await this.repository.sequelize.query(`
            select "from" as address, sum(value) + sum("gasPrice") as value
            from transactions
            where "blockId" between (select max("blockId") from transactions) - ${COUNT_LAST_BLOCK} and (select max("blockId") from transactions)
            group by "from"
            order by value desc
            limit 1;
        `,
            {type: QueryTypes.SELECT},
        );

        const resultTo: TAddress[] = await this.repository.sequelize.query(`
            select "to" as address, sum(value) as value
            from transactions
            where "blockId" between (select max("blockId") from transactions) - ${COUNT_LAST_BLOCK} and (select max("blockId") from transactions)
            group by "to"
            order by value desc
            limit 1;
        `,
            {type: QueryTypes.SELECT},
        );

        const txFrom    =   get(resultFrom, '0');
        const txTo      =   get(resultTo, '0');

        return Number(txFrom.value) > Number(txTo.value) ? txFrom : txTo;
    }

}
