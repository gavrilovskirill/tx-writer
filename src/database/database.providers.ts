import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '../shared/config/config.service';
import { Transaction } from '../shared/entity/Transaction';
import { Block } from '../shared/entity/Block';

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async (configService: ConfigService) => {
            const sequelize = new Sequelize(configService.sequelizeOrmConfig);
            sequelize.addModels([
                Transaction,
                Block,
            ]);
            await sequelize.sync();
            return sequelize;
        },
        inject: [ConfigService],
    },
];
