/* eslint-disable lines-between-class-members */
import { get } from 'lodash';
import HttpClient from '../../shared/http-client';
import { DefaultError } from '../../shared/create-error';

export type TxWriterAPIClientProps = {
  apiKey: string;
};

class TxWriterAPIClient {
  apiKey: string;
  client: HttpClient;

  constructor(props: TxWriterAPIClientProps) {
    const { apiKey } = props;
    //
    if (!apiKey) {
      throw new Error('Your not API KEY"');
    }
    //
    this.apiKey = apiKey;
    this.client = new HttpClient({

      baseURL: 'https://api.etherscan.io/api',
      timeout: 30000,
      threads: 10,
      cachePeriod: 10000,
      logEachErrorToSentry: false,

      getDataFromResponse(response) {

        const error = get(response, 'data.error');
        const result = get(response, 'data.result');

        if (!error && result) { return result;  }
        if (error) { throw error; }
        throw response;
      },

    });
  }

  async request(action, module, params = {}) {
    //
    try {
      return await this.client.sendRequest('get', '/', {
        action,
        module,
        apiKey: this.apiKey,
        ...params,
      });
    } catch (e) {
      throw new DefaultError(e.message, {
        meta: {
          method: 'GET',
          url: `${this.client.baseURL}/${action}`,
          module,
          params,
        },
        originalError: e,
      });
    }
  }

  async getBlockByNumber({ tag = 'latest', isBoolean = true }) {
    return await this.request('eth_getBlockByNumber', 'proxy', {
      tag,
      boolean: isBoolean,
    });
  }
}

export default TxWriterAPIClient;
