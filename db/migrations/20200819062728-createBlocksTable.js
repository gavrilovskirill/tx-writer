'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      CREATE TABLE IF NOT EXISTS blocks (
        id SERIAL NOT NULL PRIMARY KEY,
        "blockNumber" integer NOT NULL,
        "blockHash" character varying(255) NOT NULL
      );
    `);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query('DROP TABLE blocks;');
  }
};
