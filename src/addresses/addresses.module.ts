import { Module } from '@nestjs/common';
import { AddressesController } from './addresses.controller';
import { AddressesService } from './addresses.service';
import { TransactionsProviders } from '../shared/providers/transactions.providers';
import { ConfigService } from '../shared/config/config.service';

@Module({
  controllers: [AddressesController],
  providers: [AddressesService, ...TransactionsProviders, ConfigService],
  exports: [AddressesService]
})
export class AddressesModule {}
