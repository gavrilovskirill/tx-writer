import { Injectable, Logger } from '@nestjs/common';
import { get } from 'lodash';
import { ConfigService } from '../../shared/config/config.service';
import { TransactionService } from './transaction.service';
import { toHex } from 'web3-utils';
import { Interval } from '@nestjs/schedule';
import TxWriterAPIClient from './tx-writer-API-client';

const START_BLOCK = 9842805;
let createFlag = true;

@Injectable()
export class TxWriterService {
    private readonly logger = new Logger(TxWriterService.name);

    constructor(
        private readonly configService: ConfigService,
        private readonly service: TransactionService,
    ) {}


    @Interval(1000)
    async handleCron() {
        if (createFlag) {
            createFlag = false;
            this.logger.debug('[START PARSE BLOCK]');
            try {
                const api = new TxWriterAPIClient({
                    apiKey: this.configService.etherscanApiKey,
                });
                const block = await this.service.getLast();
                const blockNumber = get(block, 'blockNumber')
                    ? get(block, 'blockNumber') + 1
                    : START_BLOCK;
                const blockX = await api.getBlockByNumber({
                    tag: toHex(blockNumber),
                });
                await this.service.addBlock(blockX);
                this.logger.log(
                    `[PARSE BLOCK] ${blockNumber}[${toHex(blockNumber)}] - ${blockX.transactions.length} transactions`
                );
            } catch (e) {
                this.logger.error(`[ERROR] ${e.message}`);
                throw new Error(e);
            }
            setTimeout(()=>{createFlag = true}, 0);
        }
    }
}
