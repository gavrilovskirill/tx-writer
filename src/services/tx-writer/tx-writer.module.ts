import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TxWriterService } from './tx-writer.service';
import { ConfigService } from '../../shared/config/config.service';
import { databaseProviders } from '../../database/database.providers';
import { TransactionService } from './transaction.service';

@Module({
  providers: [
    ...databaseProviders,
    ConfigService,
    TxWriterService,
    TransactionService
  ],
  imports: [ScheduleModule.forRoot()],
})
export class TxWriterModule {}
