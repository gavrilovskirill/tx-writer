import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { get, pick, compact } from 'lodash';
//

//
const { NODE_ENV, SENTRY_DSN, SENTRY_ENV, HOST } = process.env;

type ErrorProps = {
  modelName?: string;
  group?: string;
  status?: number;
  originalError?: Error;
  originalException?: Error;
  meta?: any;
};

type SentryErrorProps = {
  messagePrefix?: string;
  request?: Request | AxiosRequestConfig;
  response?: Response | AxiosResponse;
  path?: string;
  transactionId?: string;
  coinId?: string;
};

export class DefaultError extends Error {
  constructor(message: string, props: ErrorProps = {}) {
    super(message); // 'Error' breaks prototype chain here
    this.name = new.target.prototype.constructor.name;
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    //
    if (props.originalError instanceof Error) {
      this.stack = props.originalError.stack;
      this.originalError = props.originalError;
    }
    this.modelName = props.modelName || '';
    this.group = props.group || '';
    this.status = props.status || 500;
    this.data = props.meta || {};
  }

  group: string | undefined;

  modelName: string | undefined;

  status: number;

  data: Object;

  originalError: Error | void;

  originalException: Error | void;
}

export default function createError(
  message: DefaultError | string,
  opts: ErrorProps = {},
) {
  //
  if (message instanceof Error) {
    return message;
  }
  return new DefaultError(message, opts);
}
