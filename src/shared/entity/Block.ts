import { Column, HasMany, Model, Table } from 'sequelize-typescript';
import { Transaction } from './Transaction';

@Table({
    tableName: 'blocks',
})
export class Block extends Model<Block> {
    @Column({
        autoIncrement: true,
        primaryKey: true,
    })
    id: number;

    @Column
    blockNumber: number;

    @Column
    blockHash: string;

    @HasMany(() => Transaction)
    transactions: Transaction[]
}
