'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      CREATE TABLE IF NOT EXISTS transactions (
        id SERIAL NOT NULL PRIMARY KEY,
        "blockId" integer NOT NULL,
        "hash" character varying(255) NOT NULL,
        "from" character varying(255) NOT NULL,
        "to" character varying(255) DEFAULT NULL,
        "value" decimal(18, 9) NOT NULL,
        "gasPrice" decimal(18, 9) default 0,
        "createdAt" timestamp with time zone NOT NULL,
        "updatedAt" timestamp with time zone NOT NULL
      );
      ALTER TABLE "transactions" ADD CONSTRAINT "transactions_blockFrom_fkey" 
      FOREIGN KEY ("blockId") REFERENCES "blocks"(id);
    `);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      ALTER TABLE transactions DROP CONSTRAINT "transactions_blockFrom_fkey";
      DROP TABLE transactions;
    `);
  }
};
