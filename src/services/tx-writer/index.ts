import { Bootstrap } from '../Bootstrap';
import { Config } from './config';
import {TxWriterModule} from './tx-writer.module';

Bootstrap.context(TxWriterModule, Config);
