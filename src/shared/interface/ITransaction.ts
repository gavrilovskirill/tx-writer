export interface ITransaction {

  blockId: number;

  hash: string;

  from: string;

  to: string;

  value: string;

  gasPrice: string;
}
