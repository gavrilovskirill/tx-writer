import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddTransaction {
    @ApiProperty()
    @IsNumber()
    blockId: number;

    @ApiProperty()
    @IsString()
    hash: string;

    @ApiProperty()
    @IsString()
    from: string;

    @ApiProperty()
    @IsString()
    to: string;

    @ApiProperty()
    @IsString()
    value: string;

    @ApiProperty()
    @IsString()
    gasPrice: string;
}
