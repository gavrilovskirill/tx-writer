import { get } from 'lodash';
import { Transaction } from '../../shared/entity/Transaction';
import { Block } from '../../shared/entity/Block';
import { ITransaction } from '../../shared/interface/ITransaction';
import { Decimal } from 'decimal.js';
import { toDecimal } from 'web3-utils';
import { IBlock } from '../../shared/interface/IBlock';

const wei = 1000000000000000000;

export type EtherscanBlock = {
    hash: string,
    number: string,
    transactions: EtherscanTransaction[],
}

export type EtherscanTransaction = {
    blockHash: string;
    blockNumber: string;
    from: string;
    gas: string;
    gasPrice: string;
    hash: string;
    input: string;
    nonce: string;
    to: string;
    transactionIndex: string;
    value: string;
    v: string;
    r: string;
    s: string;
};



export class TransactionService {
    async getLast() {
        return Block.findOne({
            order: [['blockNumber', 'DESC']],
        });
    }

    async addBlock(block: EtherscanBlock) {
        try {
            const formatBlock = this.formatBlock(block);
            const {id} = await Block.create(formatBlock);
            return await this.addTx(block.transactions, id);
        } catch (e) {
            throw new Error(e)
        }
    }

    async addTx(txList: EtherscanTransaction[], blockId) {
        try {
            const formatTxs = txList.map(tx => this.formatTx(tx, blockId));
            return await Transaction.bulkCreate(formatTxs);
        } catch (e) {
            throw new Error(e);
        }
    }

    formatBlock(block: EtherscanBlock): IBlock {
        return {
            blockNumber: toDecimal(get(block, 'number')),
            blockHash: get(block, 'hash'),
        }
    }

    formatTx(tx: EtherscanTransaction, blockId: number): ITransaction {
        return {
            blockId,
            from: get(tx, 'from', ''),
            gasPrice: this._formatAmount(get(tx, 'gasPrice')),
            hash: get(tx, 'hash'),
            to: get(tx, 'to', ''),
            value: this._formatAmount(get(tx, 'value')),
        };
    }

    _formatAmount(amount: string) {
        return new Decimal(amount).div(wei).toFixed(8);
        // const v = toBN(fromWei(`${hexToNumber(amount)}`)).toNumber();
    }
}
